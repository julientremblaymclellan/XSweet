<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [directory-manifest.xsl](#directory-manifestxsl)
- [docx-query.xsl](#docx-queryxsl)
- [html-to-markdown.xsl](#html-to-markdownxsl)
- [manifest-reorder.xsl](#manifest-reorderxsl)
- [directory-manifest-produce.xpl](#directory-manifest-producexpl)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->



#### directory-manifest.xsl

XSLT stylesheet version 3.0 (13 templates)

Runtime parameter ``dirpath`` as xs:string

#### docx-query.xsl

XSLT stylesheet version 2.0 (1 template)

Runtime parameter ``docx-file-uri`` as xs:string

#### html-to-markdown.xsl

XSLT stylesheet version 3.0 (13 templates)

#### manifest-reorder.xsl

XSLT stylesheet version 3.0 (2 templates)

#### directory-manifest-produce.xpl

XProc pipeline version 1.0 (3 steps)

Runtime dependency: `directory-manifest.xsl`

Runtime dependency: `directory-manifest.xsl`

Runtime dependency: `manifest-reorder.xsl`

Runtime dependency: `html-to-markdown.xsl`