<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [css-abstract.xsl](#css-abstractxsl)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->



#### css-abstract.xsl

XSLT stylesheet version 2.0 (6 templates)

XSweet: rewrites CSS, doing its best to promote CSS settings from style attributes to classes

Input: An HTML Typescript file.

Output: A copy, except rewritten wrt use of @style and @class.