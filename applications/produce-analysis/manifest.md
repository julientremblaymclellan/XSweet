<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [html-analysis.xsl](#html-analysisxsl)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->



#### html-analysis.xsl

XSLT stylesheet version 2.0 (4 templates)

XSweet: Digests into a single-file "report" format an HTML file, to show its output (result structures) for diagnostics.

Input: An HTML file probably "HTML Typescript" in spirit (i.e. the fairly cleanish output of an XSweet extraction pipeline).

Output: An HTML file (open in a plain-ordinary browser) documenting a 'structural synopsis' of the HTML as given.

Note: For diagnostic purposes, this is rude but useful. Also could be extended and polished for better results.